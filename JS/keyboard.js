// Favre Alan
// T.IS-E2A
// Travail de diplôme
// 2018
// WebDJ
// Fichier keyboard.js

// Arry that will contain all the IDs of the DOM elements
var calls = [];

// Fill the array with the IDs
fillCallsArray();

function handleKeys(e)
{
    // Call the onmousedown event on the correct element
    $(calls[e.keyCode]).trigger("onmousedown");
}

function fillCallsArray()
{
    calls[226] = "#l-play-button";
    calls[86] = "#r-play-button";

    calls[88] = "#left-filter-high";
    calls[78] = "#right-filter-high";

    calls[89] = "#left-filter-low";
    calls[66] = "#right-filter-low";

    calls[67] = "#l-on-button";
    calls[77] = "#r-on-button";

    calls[49] = "#effect-1";
    calls[50] = "#effect-2";
    calls[51] = "#effect-3";

    calls[65] = "#c-left-1";
    calls[83] = "#c-left-2";
    calls[68] = "#c-left-3";
    calls[70] = "#c-left-4";

    calls[81] = "#l-left-1";
    calls[87] = "#l-left-2";
    calls[69] = "#l-left-3";
    calls[82] = "#l-left-4";

    calls[71] = "#c-right-1";
    calls[72] = "#c-right-2";
    calls[74] = "#c-right-3";
    calls[75] = "#c-right-4";

    calls[84] = "#l-right-1";
    calls[90] = "#l-right-2";
    calls[85] = "#l-right-3";
    calls[73] = "#l-right-4";
}