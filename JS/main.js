// Favre Alan
// T.IS-E2A
// Travail de diplôme
// 2018
// WebDJ
// Fichier : main.js

// Tuna effects created and ready
var tuna_effects_created = false;

// Timer to update the view
timer = window.setInterval(updateApp, 20);

// Knowing if shift is pressed or not
var shifted;
$(document).on('keyup keydown', function(e){shifted = e.shiftKey} );

// Arrays
var decks = []; // Contains the wavesurfer objects
var bpm = []; // Contains the BPM of the tracks
var cues = []; // Contains the hot-cues times, default value is -1
var cues_letters = []; // Contains the letter associated to the hot-cue
var cues_buttons = []; // Contains the IDs of the DIVs containing the hot cues buttons
var cues_marker_containers = []; // Contains the IDs of the DIVs where the markers will be placed
var cues_marker_ids = []; // Contains the IDs of the spans where the letter are placed below the waveform
var mute_values = []; // Contains the values of the input range for which the deck must be muted
var time_total_spans = []; // Contains the name of the spans containing the total time
var time_now_spans = []; // Contains the name of the spans containing the time remaining
var play_pause_divs = []; // Contains the name of the divs containing the play / pause icons
var turntable_divs = []; // Contains the name of the divs containing the turntables
var turntable_angles = []; // Contains the angles of the turntables
var turntable_speeds = []; // Contains the speeds of the turntables
var tuna_objects = []; // Contains the tuna instances
var effects_objects = [
    [],
    []
]; // Contains the effects instances
var effects = []; // Contains true or false value determining if the effect is activated or not
var effects_id = []; // Contains the names of the divs containing the effects buttons
var effects_on = []; // Contains true or false value determining if the button on is activated or not
var effects_on_id = []; // Contains the names of the divs containing the on buttons
var filters_on = [
    [],
    []
]; // Contains true or false to know if the filter is activated
var filters_id = [
    [],
    []
]; // Contains the IDs of the filters buttons
var filters_objects = [
    [],
    []
]; // Contains the tuna objects of the filters
var effects_to_apply = []; // Contains the tuna object to apply to the wavesurfer object
var loops_on = [
    [],
    []
]; // Contains true or false value to know if a loop is on or not
var loops_divs = [
  [],
  []
]; // Contains the IDs of the loops butons
var loops_start = []; // Contains the starting time of the loops
var loops_durations = []; // Contains the loops duration, value is in beat
var screens_spans = [
  [],
  []
]; // Contains the IDs of the spans containing the tracks infos
var infos_id = []; // Contains the IDs of the buttons which returns to the infos screen
var faders_volume_ids = []; // Contains the IDs of the faders used to control the volume
var colors = []; // Contains the colors of the buttons for each deck
var eq_values = []; // Contains the values for the creation of the equalizers
var eq_filters = [
    [],
    []
]; // Contains the BiquadFilter used for the equalizers
var waveform_ids = []; // Contains the IDs of the divs containing the waveforms

// Initialization of the app
function init()
{
    // Fill the arrays
    fillArrays();

    // Create the database
    createDatabase();

    // Display the tracks on the browse screens
    displayTracks();

    // Create the wavesurfer instances
    createWavesurfers();

    // Subscribe the wavesurfer instances to the ready event
    subscribeEvents();

    // Display correct background in loop buttons
    handleLoopsButtons();

    // Load the files
    loadTrack(0, 1);
    loadTrack(1, 2);

    // Set both the volumes to zero
    decks[0].setVolume(0);
    decks[1].setVolume(0);
}

// Create the wavesurfer objects
function createWavesurfers()
{
    // Create left wavesurfer object
    decks[0] = WaveSurfer.create({
        container: '#l-track-waveform',
        waveColor: 'whitesmoke',
        progressColor: 'red',
        height: 90,
        barWidth: 2,
        cursorColor: 'red',
        cursorWidth: 2,
        backend: 'WebAudio'
    });

    // Create right wavesurfer object
    decks[1] = WaveSurfer.create({
        container: '#r-track-waveform',
        waveColor: 'whitesmoke',
        progressColor: 'blue',
        height: 90,
        barWidth: 2,
        cursorColor: 'blue',
        cursorWidth: 2,
        backend: 'WebAudio'
    });
}

// Subscribe the wavesurfer objects to events
function subscribeEvents()
{
    // When tracks are loaded
    decks[0].on('ready', function() {
        // Display the total time of the track
        document.getElementById(time_total_spans[0]).innerHTML = new Date(decks[0].getDuration() * 1000).toISOString().substr(14, 5);
        // If not created, create the tuna instance for that deck
        if(!tuna_effects_created)
        {
            tuna_objects[0] = new Tuna(decks[0].backend.ac);
        }
    });

    decks[1].on('ready', function() {
        // Display the total time of track
        document.getElementById(time_total_spans[1]).innerHTML = new Date(decks[1].getDuration() * 1000).toISOString().substr(14, 5);
        // If not created, create the tuna instance for that deck
        if(!tuna_effects_created)
        {
            tuna_objects[1] = new Tuna(decks[1].backend.ac);

            // Create the effects - 250 ms wait to be sure that the tuna instances are ready
            setTimeout(function() { createEffects(); }, 250);

            // Create the equalizers - 250 ms wait to be sure that the tuna instances are ready
            setTimeout(function() { createEqualizers(); }, 250);
        }
    });

    // For both decks
    for(var deck = 0; deck < decks.length; deck++)
    {
        // When the user clicks on a waveform, delete loopès
        decks[deck].drawer.on('click', function (e) {
            deleteLoops(e.path[1].id);
        });

        // When some audio is playing, handle the loops
        decks[deck].on('audioprocess', function () {
            handleLoops();
        });
    }
}

// Load a track
function loadTrack(deck, index)
{
    // Get the track info
    var track = getTrack(index);

    // Clear the waveform as if a zero-length audio is loaded
    decks[deck].empty();

    // Delete loops
    deleteLoops(waveform_ids[deck]);

    // Delete hot-cues
    deleteHotCues(deck);

    // Load the new track
    decks[deck].load("./files/" + track.file_name);

    // Insert right BPM into the array
    bpm[deck] = track.bpm;

    // Update the screens
    updateInfos(deck, track);
}

// Update the screens with new informations
function updateInfos(deck, track)
{
    // Update all the informations
    document.getElementById(screens_spans[deck][0]).src = track.cover;
    document.getElementById(screens_spans[deck][1]).innerHTML = track.title;
    document.getElementById(screens_spans[deck][2]).innerHTML = track.artist;
    document.getElementById(screens_spans[deck][3]).innerHTML = track.album;
    document.getElementById(screens_spans[deck][4]).innerHTML = track.year;
    document.getElementById(screens_spans[deck][5]).innerHTML = track.genre;
    document.getElementById(screens_spans[deck][6]).innerHTML = bpm[deck];

    // Get back to the main screen
    switchBrowse(deck, infos_id[deck]);
}

// Play / pause track
function audioAction(deck)
{
    // Play / pause track
    decks[deck].playPause();
}

// Change the volume of a track
function changeVolume(deck, value)
{
    // If the deck isn't muted, change the volume
    if(!decks[deck].getMute())
    {
        decks[deck].setVolume(value);
    }
}

// Mute or unmute a track
function setMute(value)
{
    // If the crossfader is totally on one side, mute the opposite deck
    for(var deck = 0; deck < decks.length; deck++)
    {
        if(value == mute_values[deck])
        {
            decks[deck].setMute(true);
        }
        else
        {
            decks[deck].setMute(false);

            // Update the volume
            changeVolume(deck, document.getElementById(faders_volume_ids[deck]).value);
        }
    }
}

// Change the pitch of a track
function changePitch(deck, value)
{
    // Change the audio pitch
    decks[deck].setPlaybackRate(value);

    // Change the turntable speed
    turntable_speeds[deck] = Math.round(value * 10) / 10;

    // Update the BPM in the screen
    document.getElementById(screens_spans[deck][6]).innerHTML = Math.round(bpm[deck] * value);
}

// Enable or delete a loop on a track
function toggleLoop(deck, number)
{
    // If the loop is on, disable it
    if(loops_on[deck][number])
    {
        loops_on[deck][number] = false;
    }
    else
    {
        // Set all loop table to false
        setAll(loops_on[deck], false, loops_on[deck].length);

        // Get the right loop on true
        loops_on[deck][number] = true;

        // Get the current time as start of the loop
        loops_start[deck] = decks[deck].getCurrentTime();
    }

    // Buttons display
    handleLoopsButtons();
}

// Delete the loops on one deck
function deleteLoops(id)
{
    for(var deck = 0; deck < decks.length; deck++)
    {
        if(id == waveform_ids[deck])
        {
            setAll(loops_on[deck], false, 4);
        }
    }

    // Buttons display
    handleLoopsButtons();
}

// When a hot cue button is pressed
function toggleHotCue(deck, number, element)
{
    // If the cue already exists
    if(cues[number] != -1)
    {
        // If shift is pressed
        if(shifted)
        {
            // Back to original cue point
            cues[number] = -1;

            // Back to original button color
            $(element).css("background", "radial-gradient(whitesmoke 5%, darkgrey 95%)");

            // Delete marker under the waveform
            document.getElementById(cues_marker_ids[number]).remove();
        }
        else
        {
            // Go to cue time
            decks[deck].play(cues[number]);

            // Change loop start time
            loops_start[deck] = cues[number];

            // Change deck angle
            changeAngle(deck);
        }
    }
    else
    {
        // Get actual time and percentage of track already played
        cues[number] = decks[deck].getCurrentTime();
        var percentage = Math.round(decks[deck].getCurrentTime() * 100 / decks[deck].getDuration()) - 1;

        // Change the button background and add the marker below the waveform
        $(element).css("background", colors[deck]);
        document.getElementById(cues_marker_containers[deck]).innerHTML += "<span id='" + cues_marker_ids[number] + "' style='position: absolute; left:" + percentage + "%;'><b>" + cues_letters[number] + "</b></span>";
    }
}

function deleteHotCues(deck)
{
    var start_values = [];
    var end_values = [];

    start_values[0] = 0;
    start_values[1] = 4;
    end_values[0] = 3;
    end_values[1] = 7;

    for(var i = start_values[deck]; i <= end_values[deck]; i++)
    {
        cues[i] = -1;
        document.getElementById(cues_buttons[i]).style.background = "radial-gradient(whitesmoke 5%, darkgrey 95%)";

        if(document.getElementById(cues_marker_ids[i]) != null)
        {
            document.getElementById(cues_marker_ids[i]).remove();
        }
    }
}

function checkIfFalse(element)
{
    // Returns the opposite of the parameter given
    return !element;
}

function setAll(array, value, length)
{
    // Set all array cell to a certain value
    for(var i = 0; i < length; i++)
    {
        array[i] = value;
    }
}

// When an effect button is pressed
function handleEffect(number)
{
    // If the effect is on, disable it
    if(effects[number])
    {
        effects[number] = false;
    }
    else
    {
        // Set whole array to false (only one effect at a time)
        setAll(effects, false, 4);

        // Set the right effect to true
        effects[number] = true;
    }

    // If all elements are false, it means that no effect is clicked
    if(effects.every(checkIfFalse))
    {
        effects[3] = true;
    }

    // Apply effect or filter
    handleEffectsFilters();

    // Buttons display
    handleEffectsButtons();
}

// When the ON button is pressed
function effectOn(deck)
{
    // Switch on / off
    effects_on[deck] = !effects_on[deck];

    // Change filter status
    handleFilter(deck);

    // Buttons display
    handleEffectsOnButtons();
}

// When a HI or LOW filter is pressed
function handleFilter(deck, value)
{
    // If the on effect button is clicked, set both filters to false
    if(effects_on[deck])
    {
        setAll(filters_on[deck], false, 2);
    }
    else
    {
        for (var i = 0; i < filters_on.length; i++)
        {
            if(deck == i)
            {
                // If the filter is on, disable it
                if(filters_on[i][value])
                {
                    filters_on[i][value] = false;
                }
                else
                {
                    // Set all filters to false
                    setAll(filters_on[i], false, 2);

                    // Set the right filter to true
                    filters_on[i][value] = true;
                }
            }
        }
    }

    // Apply effect or filter
    handleEffectsFilters();

    // Buttons display
    handleFiltersButtons();
}

// Get which effect should be applied to the deck
function handleEffectsFilters()
{
    for(var deck = 0; deck < decks.length; deck++)
    {
        // If the ON button is clicked
        if(effects_on[deck])
        {
            for(var e = 0; e < effects.length; e++)
            {
                if(effects[e])
                {
                    // Put correct tuna object in effects_to_apply array
                    effects_to_apply[deck] = effects_objects[deck][e];
                }
            }
        }
        else
        {
            // If one filter is clicked
            if(filters_on[deck][0] || filters_on[deck][1])
            {
                for(var e = 0; e < filters_on[deck].length; e++)
                {
                    if(filters_on[deck][e])
                    {
                        // Put the filter object in effects_to_apply array
                        effects_to_apply[deck] = filters_objects[deck][e];
                    }
                }
            }
            else
            {
                // Object which doesn't affect the sound
                effects_to_apply[deck] = effects_objects[deck][3];
            }
        }
    }

    // Apply the filters on the audio
    applyAudioFilters();
}

// Apply the filters on the audio
function applyAudioFilters()
{
    for(var deck = 0; deck < decks.length; deck++)
    {
        // Add the effect or filter to the equalizer
        eq_filters[deck][3] = effects_to_apply[deck];

        // Apply all the effect present in the eq_filters array
        decks[deck].backend.setFilters(eq_filters[deck]);
    }
}

// When a knob is moved
function handleKnob(deck, knob, value)
{
    // Change the value of the gain for the correct BiquadFilter
    eq_filters[deck][knob].gain.value = value.toFixed(2) * 100;

    // Apply the filters on the audio
    applyAudioFilters();
}

// Update the app
function updateApp()
{
    for(var deck = 0; deck < decks.length; deck++) {
        // Current time of song
        document.getElementById(time_now_spans[deck]).innerHTML = new Date(decks[deck].getCurrentTime() * 1000).toISOString().substr(14, 5);

        // Play pause button & turntables turning
        if (decks[deck].isPlaying()) {
            if (turntable_angles[deck] < 360) {
                turntable_angles[deck] += turntable_speeds[deck] * 2;
            }
            else {
                turntable_angles[deck] = 0;
            }

            document.getElementById(turntable_divs[deck]).style.transform = "rotate(" + turntable_angles[deck] + "deg)";

            document.getElementById(play_pause_divs[deck]).innerHTML = '<b><i class="material-icons">pause</i></b>';
        }
        else {
            document.getElementById(play_pause_divs[deck]).innerHTML = '<b><i class="material-icons">play_arrow</i></b>';
        }
    }
}

// Handle the display of the ON buttons
function handleEffectsOnButtons()
{
    for(var deck = 0; deck < decks.length; deck++)
    {
        // Effects ON divs
        if (effects_on[deck]) {
            document.getElementById(effects_on_id[deck]).style.background = "radial-gradient(#99ff99 10%, #1aff1a 95%)";
        }
        else {
            document.getElementById(effects_on_id[deck]).style.background = "radial-gradient(#00e600 5%, #008000 95%)";
        }
    }
}

// Handle the loops
function handleLoops()
{
    for(var deck = 0; deck < decks.length; deck++)
    {
        for(var i = 0; i < loops_on[deck].length; i++)
        {
            // If a loop is activated
            if(loops_on[deck][i])
            {
                // If the actual time is greater than the start time of the loop + the duration of the loop
                if(decks[deck].getCurrentTime() >= loops_start[deck] + loops_durations[i] * 1 / bpm[deck] * 60)
                {
                    // Go back to the start of the loop
                    decks[deck].play(loops_start[deck]);

                    // Change angle to give the impression that the track went backwards
                    changeAngle(deck);
                }
            }
        }
    }
}

// Change angle of deck to give the impression that the track went backwards
function changeAngle(deck)
{
    var angle_offset = 60;

    if(turntable_angles[deck] - angle_offset < 0)
    {
        turntable_angles[deck] -= angle_offset + 360;
    }
    else
    {
        turntable_angles[deck] -= angle_offset;
    }
}

// Handle the display of the effects buttons
function handleEffectsButtons()
{
    for(var i = 0; i < effects_id.length; i++)
    {
        if (effects[i])
        {
            document.getElementById(effects_id[i]).style.background = "radial-gradient(#99ff99 5%, #1aff1a 95%)";
        }
        else
        {
            document.getElementById(effects_id[i]).style.background = "radial-gradient(#00e600 5%, #008000 95%)";
        }
    }
}

// Handle the display op the filters buttons
function handleFiltersButtons()
{
    for(var i = 0; i < filters_id.length; i++)
    {
        for(var j = 0; j < filters_id[i].length; j++)
        {
            if(filters_on[i][j])
            {
                document.getElementById(filters_id[i][j]).style.background = "radial-gradient(#99ff99 10%, #1aff1a 95%)";
            }
            else
            {
                document.getElementById(filters_id[i][j]).style.background = "radial-gradient(#00e600 5%, #008000 95%)";
            }
        }
    }
}

// Handle the display op the loops buttons
function handleLoopsButtons()
{
    for(var i = 0; i < loops_on.length; i++)
    {
        for(var j = 0; j < loops_on[i].length; j++)
        {
            if(loops_on[i][j])
            {
                document.getElementById(loops_divs[i][j]).style.background = colors[i];
            }
            else
            {
                document.getElementById(loops_divs[i][j]).style.background = "radial-gradient(whitesmoke 5%, darkgrey 95%)";
            }
        }
    }
}

// Create all the tuna objects which will serve to apply effects
function createEffects()
{
    for(var i = 0; i < decks.length; i++)
    {
        effects_objects[i][0] = new tuna_objects[i].Chorus({
            rate: 1.5,         //0.01 to 8+
            feedback: 0.2,     //0 to 1+
            delay: 0.0045,     //0 to 1
            bypass: 0          //the value 1 starts the effect as bypassed, 0 or 1
        });

        effects_objects[i][1] = new tuna_objects[i].MoogFilter({
            cutoff: 0.065,    //0 to 1
            resonance: 3.5,   //0 to 4
            bufferSize: 4096  //256 to 16384
        });

        effects_objects[i][2] = new tuna_objects[i].Bitcrusher({
            bits: 4,          //1 to 16
            normfreq: 0.1,    //0 to 1
            bufferSize: 4096  //256 to 16384
        });

        effects_objects[i][3] = new tuna_objects[i].Gain({
            gain: 1 // 0 and up
        });

        filters_objects[i][0] = new tuna_objects[i].Filter({
            frequency: 440, //20 to 22050
            Q: 1, //0.001 to 100
            gain: 0, //-40 to 40 (in decibels)
            filterType: "highpass", //lowpass, highpass, bandpass, lowshelf, highshelf, peaking, notch, allpass
            bypass: 0
        });

        filters_objects[i][1] = new tuna_objects[i].Filter({
            frequency: 440, //20 to 22050
            Q: 1, //0.001 to 100
            gain: 0, //-40 to 40 (in decibels)
            filterType: "lowpass", //lowpass, highpass, bandpass, lowshelf, highshelf, peaking, notch, allpass
            bypass: 0
        });
    }

    // The tuna effects are ready
    tuna_effects_created = true;
}

// Fill the arrays with the correct data
function fillArrays()
{
    mute_values[0] = 2;
    mute_values[1] = 0;

    time_total_spans[0] = "left-time-total";
    time_total_spans[1] = "right-time-total";

    time_now_spans[0] = "left-time-now";
    time_now_spans[1] = "right-time-now";

    play_pause_divs[0] = "left-play-inside";
    play_pause_divs[1] = "right-play-inside";

    turntable_divs[0] = "left-turntable";
    turntable_divs[1] = "right-turntable";

    turntable_angles[0] = 0;
    turntable_angles[1] = 0;

    turntable_speeds[0] = 1;
    turntable_speeds[1] = 1;

    setAll(cues, -1, 8);

    setAll(loops_on[0], false, 4);

    setAll(loops_on[1], false, 4);

    cues_letters[0] = "A";
    cues_letters[1] = "B";
    cues_letters[2] = "C";
    cues_letters[3] = "D";
    cues_letters[4] = "A";
    cues_letters[5] = "B";
    cues_letters[6] = "C";
    cues_letters[7] = "D";

    cues_buttons[0] = "c-left-1";
    cues_buttons[1] = "c-left-2";
    cues_buttons[2] = "c-left-3";
    cues_buttons[3] = "c-left-4";
    cues_buttons[4] = "c-right-1";
    cues_buttons[5] = "c-right-2";
    cues_buttons[6] = "c-right-3";
    cues_buttons[7] = "c-right-4";

    colors[0] = "radial-gradient(#ff6666 5%, #ff0000 95%)";
    colors[1] = "radial-gradient(#4d79ff 5%, #0039e6 95%)";

    cues_marker_containers[0] = "l-track-hot-cues";
    cues_marker_containers[1] = "r-track-hot-cues";

    cues_marker_ids[0] = "left-A";
    cues_marker_ids[1] = "left-B";
    cues_marker_ids[2] = "left-C";
    cues_marker_ids[3] = "left-D";
    cues_marker_ids[4] = "right-A";
    cues_marker_ids[5] = "right-B";
    cues_marker_ids[6] = "right-C";
    cues_marker_ids[7] = "right-D";

    effects[0] = false;
    effects[1] = false;
    effects[2] = false;
    effects[3] = true;

    effects_id[0] = "effect-1";
    effects_id[1] = "effect-2";
    effects_id[2] = "effect-3";

    effects_on[0] = false;
    effects_on[1] = false;

    effects_on_id[0] = "l-on-button";
    effects_on_id[1] = "r-on-button";

    filters_on[0][0] = false;
    filters_on[0][1] = false;
    filters_on[1][0] = false;
    filters_on[1][1] = false;

    filters_id[0][0] = "left-filter-high";
    filters_id[0][1] = "left-filter-low";
    filters_id[1][0] = "right-filter-high";
    filters_id[1][1] = "right-filter-low";

    loops_divs[0][0] = "l-left-1";
    loops_divs[0][1] = "l-left-2";
    loops_divs[0][2] = "l-left-3";
    loops_divs[0][3] = "l-left-4";
    loops_divs[1][0] = "l-right-1";
    loops_divs[1][1] = "l-right-2";
    loops_divs[1][2] = "l-right-3";
    loops_divs[1][3] = "l-right-4";

    loops_durations[0] = 2;
    loops_durations[1] = 4;
    loops_durations[2] = 8;
    loops_durations[3] = 16;

    screens_spans[0][0] = "left-cover";
    screens_spans[0][1] = "left-title";
    screens_spans[0][2] = "left-artist";
    screens_spans[0][3] = "left-album";
    screens_spans[0][4] = "left-year";
    screens_spans[0][5] = "left-genre";
    screens_spans[0][6] = "left-bpm";

    screens_spans[1][0] = "right-cover";
    screens_spans[1][1] = "right-title";
    screens_spans[1][2] = "right-artist";
    screens_spans[1][3] = "right-album";
    screens_spans[1][4] = "right-year";
    screens_spans[1][5] = "right-genre";
    screens_spans[1][6] = "right-bpm";

    infos_id[0] = "l-i";
    infos_id[1] = "r-i";

    faders_volume_ids[0] = "left-fader-input";
    faders_volume_ids[1] = "right-fader-input";

    waveform_ids[0] = "l-track-waveform";
    waveform_ids[1] = "r-track-waveform";

    // Values of the equalizers, f is frequency
    eq_values[0] = {f: 8000, type: 'peaking'};
    eq_values[1] = {f: 750, type: 'peaking'};
    eq_values[2] = {f: 64, type: 'peaking'};
}

// Create the equalizers objects
function createEqualizers()
{
    for(var deck = 0; deck < decks.length; deck++)
    {
        // Create the filters and put them in the eq_filters array
        eq_filters[deck] = eq_values.map(function (band){
            var filter = decks[deck].backend.ac.createBiquadFilter(); // Create a BiquadFilter using the AudioContext
            filter.type = band.type; // Choose the correct type
            filter.gain.value = 0; // Start value is 0
            filter.Q.value = 1;
            filter.frequency.value = band.f; // Choose the correct frequency
            return filter;
        });
    }

    // Call so it goes through the effects / filters handling and it applies all the filters to the audio
    handleEffectsFilters();
}