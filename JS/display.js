// Favre Alan
// T.IS-E2A
// Travail de diplôme
// 2018
// WebDJ
// Fichier display.js

// Give the visual impression the button has been clicked
function clickMimickDown(element, browse, deck, round, shortcuts)
{
    // Click effect
    $(element).css("box-shadow", "0 0 1px 2px rgba(0,0,0,0.5)");
    $(element).css("height", element.offsetHeight - 2 + "px");
    $(element).css("width", element.offsetWidth - 2 + "px");
    $(element).css("left", element.offsetLeft + 1 + "px");
    $(element).css("top", element.offsetTop + 1 + "px");

    // Round button
    if(round)
    {
        roundDown(element);
        setTimeout(function() { roundUp(element); }, 125);
    }

    // Display or hide browse / infos
    if(browse)
    {
        switchBrowse(deck, element.id);
    }

    // Display or hide shortcuts
    if(shortcuts)
    {
        switchShortcuts();
    }

    // Waiting 125 ms and calling the up effect
    setTimeout(function() { clickMimcikUp(element); }, 125);
}

// Back to normal state
function clickMimcikUp(element)
{
    $(element).css("box-shadow", "0 0 3px 4px rgba(0,0,0,0.5)");
    $(element).css("height", element.offsetHeight + 2 + "px");
    $(element).css("width", element.offsetWidth + 2 + "px");
    $(element).css("left", element.offsetLeft - 1 + "px");
    $(element).css("top", element.offsetTop - 1 + "px");
}

// Display or hide the shortcuts screen
function switchShortcuts()
{
    // Get the div containing the screen
    var div = document.getElementById("shortcuts");

    // Get the div containing the button
    var button_div = document.getElementById("shortcuts-button");

    // If the screen is visible
    if(div.style.display == "block")
    {
        // Hide it and turn button of
        div.style.display = "none";
        button_div.style.background = "radial-gradient(#00e600 5%, #008000 95%)";
    }
    else
    {
        // Show it and turn button on
        div.style.display = "block";
        button_div.style.background = "radial-gradient(#99ff99 5%, #1aff1a 95%)";
    }
}

// Hide or show infos / browse
function switchBrowse(deck, id)
{
    // Array containing the IDs of the divs containing the buttons
    var divs_ids = [
      [],
      []
    ];

    divs_ids[0][0] = "l-infos-div";
    divs_ids[0][1] = "l-browse-div";

    divs_ids[1][0] = "r-infos-div";
    divs_ids[1][1] = "r-browse-div";

    // If the button browse is clicked
    if(id.charAt(2) == "b")
    {
        // Show browse and hide infos
        document.getElementById(divs_ids[deck][0]).style.display = "none";
        document.getElementById(divs_ids[deck][1]).style.display = "block";
    }
    else
    {
        // Show infos and hide browse
        document.getElementById(divs_ids[deck][0]).style.display = "block";
        document.getElementById(divs_ids[deck][1]).style.display = "none";
    }
}

// Special code in case of a round button
function roundDown(element)
{
    // Inner ring
    $($(element).children()).css("height", "83px");
    $($(element).children()).css("width", "83px");

    // Inner circle
    $($(element).children()).children().css("height", "73px");
    $($(element).children()).children().css("width", "73px");
}

// Special code in case of a round button
function roundUp(element)
{
    // Inner ring
    $($(element).children()).css("height", "85px");
    $($(element).children()).css("width", "85px");

    // Inner circle
    $($(element).children()).children().css("height", "75px");
    $($(element).children()).children().css("width", "75px");
}