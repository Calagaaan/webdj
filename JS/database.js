// Favre Alan
// T.IS-E2A
// Travail de diplôme
// 2018
// WebDJ
// Fichier : database.js

// Variable containing the database
var tracks;

// Create DB
function createDatabase()
{
    tracks = TAFFY([
        {"id":1, "title":"Chipunch", "artist":"Buskerdroid", "bpm":128, "time":"03:41", "file_name":"chipunch.mp3", "genre":"Dance", "album":"Chipunch", "year":"2017", "cover":"https://e-cdns-images.dzcdn.net/images/cover/c41bc3b1e4e2afd48de2ceb75d4e1cfd/264x264-000000-80-0-0.jpg"},
        {"id":2, "title":"Nightrise", "artist":"Jens East", "bpm":125, "time":"03:10", "file_name":"nightrise.mp3", "genre":"House", "album":"Nightrise", "year":"2018", "cover":"https://e-cdns-images.dzcdn.net/images/cover/acb79868a46aef76c555895a21854207/264x264-000000-80-0-0.jpg"},
        {"id":3, "title":"Suffering", "artist":"Mark Radford & Jakkin Rabbit", "bpm":120, "time":"06:24", "file_name":"suffering.mp3", "genre":"House", "album":"Suffering", "year":"2017", "cover":"https://e-cdns-images.dzcdn.net/images/cover/6bb97b7d69ecb34247f3f75d3c807562/264x264-000000-80-0-0.jpg"},
        {"id":4, "title":"Dreams", "artist":"ENDRUDARK", "bpm":145, "time":"04:44", "file_name":"dreams.mp3", "genre":"House", "album":"Dreams", "year":"2016", "cover":"https://e-cdns-images.dzcdn.net/images/cover/9450f9beb442aa9cab666f34c377a504/264x264-000000-80-0-0.jpg"},
        {"id":5, "title":"Ay ay ay!", "artist":"MegaHast3r", "bpm":120, "time":"06:44", "file_name":"ayayay.mp3", "genre":"House", "album":"Ay ay ay!", "year":"2016", "cover":"https://e-cdns-images.dzcdn.net/images/cover/3488f582b7f2759fa6282b2bf7b5487f/264x264-000000-80-0-0.jpg"},
        {"id":6, "title":"Miss Japan", "artist":"MegaHast3r", "bpm":125, "time":"04:28", "file_name":"missjapan.mp3", "genre":"Techno", "album":"Miss Japan", "year":"2016", "cover":"https://e-cdns-images.dzcdn.net/images/cover/571b62c75bd37873ee65ec20df00df82/264x264-000000-80-0-0.jpg"},
        {"id":7, "title":"Powder", "artist":"Carl Phaser", "bpm":127, "time":"05:17", "file_name":"powder.mp3", "genre":"Dance", "album":"Powder", "year":"2017", "cover":"https://e-cdns-images.dzcdn.net/images/cover/ab594c828adacb03aeb94770d8a0d8b0/264x264-000000-80-0-0.jpg"},
        {"id":8, "title":"Tomb Raider (Club Mix)", "artist":"MFYP", "bpm":100, "time":"05:22", "file_name":"tombraider.mp3", "genre":"Dance", "album":"Tomb Raider", "year":"2016", "cover":"https://e-cdns-images.dzcdn.net/images/cover/56f960e8d209e16c89712072d6fb0fbc/264x264-000000-80-0-0.jpg"}
    ]);
}

// Display tracks infos into browse screens
function displayTracks()
{
    // Array containing the IDs of the table body which will contain the display
    var table_ids = [];

    table_ids[0] = "l-tracks-body";
    table_ids[1] = "r-tracks-body";

    // Get all tracks on database
    var all_tracks = tracks().get();

    // For each side
    for(var side = 0; side < 2; side++)
    {
        // Get the div into the table
        var div = document.getElementById(table_ids[side]);

        // For each track
        for(var i = 0; i < all_tracks.length; i++)
        {
            // Add a row containing the data to the div
            div.innerHTML
                += "<tr><td>"
                + all_tracks[i].artist
                + "</td><td>"
                + all_tracks[i].title
                +"</td><td style='text-align: center;'>"
                + all_tracks[i].bpm
                +"</td><td style='text-align: center;'>"
                + all_tracks[i].time
                +"</td><td>"
                + "<div class='load-button' onclick='loadTrack("+ side + "," + all_tracks[i].id +")'>Load</div>"
                +"</td></tr>"
        }
    }
}

// Get the track data with the index given by parameter
function getTrack(index)
{
    return tracks({id:index}).first();
}
